# ATRBAC Smart Contract for Ethereum

## Background

Administrative Temporal Role Based Access Control (ATRBAC) is a access control scheme that extends the very well known
access control scheme RBAC with adding temporal extensions and Administrative Extensions.

The reason ATRBAC is an interesting and important access control scheme is that it allows for access control
scaling through the use of **administrative delegation**.
Administrative delegation provides particular roles the ability to change the policy in limited ways.
This delegation allows scaling because it prevents many requests from being sent to the access control super users
(usually the IT department) and instead sends it to the content area experts.

Example environments where ATRBAC is useful are: Large Schools/Universities, Hospitals, Governments, Large Businesses,
and any access control system that must deal with timezones and sensitive information.

## Inspiration

I have been working in access control and computer security for the last 6 years.
My masters thesis is specifically about ATRBAC and how to solve a hard problem of detecting faulty or malicious policies.
My PhD thesis has 2 papers attached to it that deal directly with ATRBAC.

## Prior/Related Work

This field is very important, but there are no ATRBAC smart contracts available.
This makes this project a novel idea.

There exists a paper on how to create a RBAC smart contract (https://ieeexplore.ieee.org/abstract/document/8307397), and there is a fork of OpenZeppelin that attempted
to create a RBAC smart contract but it is unfinished
(https://github.com/OpenZeppelin/openzeppelin-contracts/tree/master/contracts/access).
A completed RBAC tutorial exists here
(https://hackernoon.com/role-based-access-control-for-the-ethereum-blockchain-bcc9dfbcfe5c) by Alberto Cuesta Cañada.

## What it does

My project recreates ATRBAC as a smart contract in Ethereum.
I will make it publically available, and thus companies/universities/governments will be able to use it to have a
distributed access control policy where concerns of malicious policy changes have a traceable and un-erasable log.

Blockchains (specifically Ethereum) and access control were built for each other. There is little security risk of
publishing your access control policy (as long as you exclude the roles -> permissions).
Ethereum accounts provide a strong association between a real person and who they claim to be
(hopefully we can trust users not to lose their wallets too frequently).

Having published access control policies can allow for outside audits, and audits can be done without access to
highly sensitive computers and software.
With public access control policies it will be much easier for new companies to get started by copying well known
access control policies in their field and using that as a starting place (i.e. a new university can reuse the
University of Waterloo's as a starting point).

## How I built it

I build this project from scratch using Solidity, mostly in VS Code but in the beginning I was using Remix.
I have setup a truffle environment and am using Mocha to right a comprehensive set of verification tests.

## Challenges I ran into

Fortunately I am very familiar with ATRBAC, so not too many issues there.

Unfortunately this weekend was a big learning curve for the Ethereum toolchain.

The biggest issues I had was figuring out I required `--reset` when running my manual `truffle migrate`,
and that I cannot get return results from a transaction, so I needed to write lots of getter functions.

## Accomplishments that I'm proud of

This is a very large project, I am surprised with how much I was able to do since I did no coding Friday.

## What I learned

This weekend has provided me with so much knowledge, but more importantly it has provided me with confidence to
tackle future ethereum projects, as I can always come back to this project to refresh any memory that might be lost
due to time.

## What's next for ATRBAC Smart Contract for Ethereum

This will be added to my PhD thesis (graduating July 2020) and will probably be attached to a paper which
I will publish at a later date.

## Technical Details 1. Temporal Extension (TRBAC State)

The temporal extensions allow for more control over when a user is allowed to change the policy and provides
an extra dimension of control for restricting how policies can be changed.

Instead of a user being enrolled into a role, they are instead enrolled into a tuple `<Role, Timeslot>`.
Where the definition of a timeslot is incremental, wrapping, and non-overlapping.

Example Timeslots:

1. A timeslot for each day of the week
1. A timeslot for each 8-hour portion of a day (remove timezones)
1. A timeslot for each fiscal quarter of the year

With this control, we allow access control to be a global and more versatile system.

## Technical Details 2. Adminstrative Extensions (Rule Set and Role Enablement)

Administrative extensions provide a rule set that dictates how the ATRBAC policy can change without super user intervention.
This allows for arbitrary roles to gain access to Assign and Revoke roles from users (under well defined circumstances) or
prevent users from accessing this ability.

Administrative extensions provides a rule set of:

- Can Assign -- assign a role to a user
- Can Revoke -- remove a role from a user
- Can Enable -- enable a role to fire rules
- Can Disable -- disable a role's ability to fire rules

# Developer Details

## Migrating to Test Blockchain

**Note:** You do not need to migrate if you are running tests (they do this for you)

1. Start Ganache (or run `truffle develop`)
1. Run `truffle migrate --reset` (the reset must be there or nothing happens)
1. Done, you can now start up the console `truffle console` and interact with the new contract
   - Make sure you kill the console and reopen it to get a new instance of the ATRBAC policy,
     otherwise you won't see the updates

## Running Tests

1. Have Ganche running
1. Run `truffle test`
1. Done
